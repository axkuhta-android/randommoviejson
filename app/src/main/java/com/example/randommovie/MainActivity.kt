package com.example.randommovie

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import com.google.gson.Gson
import java.io.InputStreamReader
import kotlin.math.floor

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val gson = Gson()
        val stream = resources.openRawResource(R.raw.movies)
        val reader = InputStreamReader(stream)
        val movies = gson.fromJson(reader, Movies::class.java).movies

        val button: Button = findViewById(R.id.button)
        val name: TextView = findViewById(R.id.movietext)
        val director: TextView = findViewById(R.id.directorText)
        val rating: TextView = findViewById(R.id.ratingText)
        val year: TextView = findViewById(R.id.yearText)
        val stars: TextView = findViewById(R.id.starsText)
        var moviesRemain = movies.size

        val find = {
            if (moviesRemain == 0) {
                name.text = getString(R.string.end)
                name.setTextColor(getColor(R.color.red))

                director.text = "-"
                rating.text = "-"
                year.text = "-"
                stars.text = "-"
            } else {
                val id = floor(Math.random() * moviesRemain).toInt()

                moviesRemain -= 1
                val t = movies[moviesRemain]
                movies[moviesRemain] = movies[id]

                name.text = movies[id].name
                director.text = movies[id].director
                rating.text = movies[id].rating.toString()
                year.text = movies[id].year.toString()
                stars.text = movies[id].stars.joinToString("\n")

                movies[id] = t
            }
        }

        find()

        button.setOnClickListener {
            find()
        }
    }
}